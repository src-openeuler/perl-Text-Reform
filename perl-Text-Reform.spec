Name:           perl-Text-Reform
Version:        1.20
Release:        1
Summary:        Manual text wrapping and reformatting
License:        GPL+ or Artistic
URL:            https://metacpan.org/release/Text-Reform
Source0:        https://cpan.metacpan.org/authors/id/C/CH/CHORNY/Text-Reform-%{version}.tar.gz
BuildArch:      noarch
# Build
BuildRequires:  coreutils
BuildRequires:  perl-interpreter
BuildRequires:  perl-generators
BuildRequires:  perl(Module::Build)
BuildRequires:  perl(strict)
# Runtime
BuildRequires:  perl(Carp)
BuildRequires:  perl(Exporter)
BuildRequires:  perl(strict)
# XXX: BuildRequires:  perl(TeX::Hyphen)
BuildRequires:  perl(vars)
# Tests only
BuildRequires:  perl(overload)
BuildRequires:  perl(Test::More)
# Optional tests only
BuildRequires:  perl(Test::Pod) >= 1.14 
Requires:       perl(:MODULE_COMPAT_%(eval "$(perl -V:version)"; echo $version))
Requires:       perl(TeX::Hyphen)

%description
The module supplies a re-entrant, highly configurable replacement for the
built-in Perl format() mechanism.

%prep
%setup -q -n Text-Reform-%{version}
chmod 644 -c Changes README lib/Text/*.pm

%build
perl Build.PL installdirs=vendor
./Build

%install
./Build install destdir=$RPM_BUILD_ROOT create_packlist=0
%{_fixperms} $RPM_BUILD_ROOT/*

%check
# the testsuite fails for locales with decimal point != ".", i.e. it
# fails for almost all European languages except en
LC_NUMERIC=C ./Build test

%files
%doc Changes README demo/
%{perl_vendorlib}/*
%{_mandir}/man3/*

%changelog
* Thu Jul 01 2021 Wang Gang <wanggang@kylinos.cn> - 1.20-1
- project init
